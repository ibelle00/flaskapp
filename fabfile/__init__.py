from fabric.decorators import task

import flask_app
import db
import puppet
import virtualenv

@task
def build():
    """Execute build tasks for all components."""
    virtualenv.build()
    db.build()

@task
def run():
    """Start app in debug mode (for development)."""
    flask_app.run()