from flaskapp import app
from flask import render_template, flash, redirect
from forms import LoginForm

OPENID_PROVIDERS = [
    { 'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id' },
    { 'name': 'Yahoo', 'url': 'https://me.yahoo.com' },
    { 'name': 'AOL', 'url': 'http://openid.aol.com/<username>' },
    { 'name': 'Flickr', 'url': 'http://www.flickr.com/<username>' },
    { 'name': 'MyOpenID', 'url': 'https://www.myopenid.com' }]

@app.route('/')
@app.route('/index')
def index():
    user = { 'nickname': 'Banjo' } # fake user
    return render_template("index.html",
        user = user)

@app.route('/index2')
def index2():
    user = { 'nickname': "Ba'lake"} # fake user
    posts = [ # fake array of posts
        { 
            'author': { 'nickname': 'John' }, 
            'body': 'Beautiful day in Portland!' 
        },
        { 
            'author': { 'nickname': 'Susan' }, 
            'body': 'The Avengers movie was so cool!' 
        }
    ]
    return render_template("index2.html",
        title = 'Home',
        user = user,
        posts = posts)

@app.route('/base')
def base():
    return render_template('base.html')


@app.route('/login',methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login Requested for OpenID %s, remember me %s' %(form.openid.data, form.remember_me.data))
        return redirect('/index')
    return render_template('login.html',title='Sign In',form=form, providers=OPENID_PROVIDERS)



 